variable "spokes" {
  type = map(object({
    cloud            = string,
    name             = string,
    region           = string,
    vpc_id           = string,
    gw_subnet_cidr   = string,
    hagw_subnet_cidr = string,
    account_name     = string,
    account_number   = optional(string),
  }))

  default = {
    # example = {
    #   cloud            = "AWS",
    #   name             = "App1",
    #   region           = "eu-central-1",
    #   vpc_id           = "xxxxx",
    #   gw_subnet_cidr   = "x.x.x.x/x",
    #   hagw_subnet_cidr = "x.x.x.x/x",
    #   account_name     = "MyAccount",     #Unique name for account for new account creation, or use existing name.
    #   account_number   = "12345678910",   #Leave blank to use earlier created account
    # },
    App1 = {
      cloud            = "AWS",
      name             = "App1",
      region           = "eu-central-1",
      vpc_id           = "vpc-0314c23a59086bc8c",
      gw_subnet_cidr   = "10.10.1.224/28",
      hagw_subnet_cidr = "10.10.1.240/28",
      account_name     = "App1",
      account_number   = "983531232307",
    },
    App2 = {
      cloud            = "AWS",
      name             = "App2",
      region           = "us-east-1",
      vpc_id           = "vpc-0b9187fe7cbf8d991",
      gw_subnet_cidr   = "10.10.2.224/28",
      hagw_subnet_cidr = "10.10.2.240/28",
      account_name     = "App1",
    },
  }
}
