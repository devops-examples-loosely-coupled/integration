locals {
  transit_map = data.tfe_outputs.core-network.values.transit_map

  spokes = defaults(var.spokes, {
    account_number = ""
  })
}

