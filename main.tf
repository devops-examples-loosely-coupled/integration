module "spokes" {
  for_each = var.spokes

  source  = "terraform-aviatrix-modules/mc-spoke/aviatrix"
  version = "1.1.2"

  cloud      = each.value.cloud
  name       = each.value.name
  region     = each.value.region
  account    = aviatrix_account.accounts[each.value.account_name].account_name
  ha_gw      = false
  transit_gw = lookup(local.transit_map, each.value.region, null)

  use_existing_vpc = true
  vpc_id           = each.value.vpc_id
  gw_subnet        = each.value.gw_subnet_cidr
  hagw_subnet      = each.value.hagw_subnet_cidr
}

resource "aviatrix_account" "accounts" {
  for_each = { for k, v in var.spokes : k => v if local.spokes[k].account_number != "" }

  account_name       = each.value.account_name
  cloud_type         = 1
  aws_account_number = each.value.account_number
  aws_iam            = true
  aws_role_app       = format("arn:aws:iam::%s:role/aviatrix-role-app", each.value.account_number)
  aws_role_ec2       = format("arn:aws:iam::%s:role/aviatrix-role-ec2", each.value.account_number)
}
