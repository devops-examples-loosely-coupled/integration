# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/aviatrixsystems/aviatrix" {
  version     = "2.21.2"
  constraints = "2.21.2, ~> 2.21.2"
  hashes = [
    "h1:YFOlfo3sTyLXGPGvBN5VYHuZLUr7P1xt4K6na8gMtLw=",
    "zh:0d0b45d96e8059e47a2b90f790e656e561d7321efa405d083316ac6cebf96deb",
    "zh:275c8d0eaccfb7a1a0cd96a240e8be7581b157b10526ae8ea404b61292c03b7f",
    "zh:2d9f56fa4b39b4512282c1b32e9259eb17ddbd26b9556e7647831cc66c028a2d",
    "zh:48081c72758234f480d06580b6affa04106a0aa312f472b09323aed950297e02",
    "zh:4ba7d5b293b61cc5dcf206ea9cd56f3131bb8b06cc74e399bd05355dbc58d154",
    "zh:524cd056c08a63f3806c6de8e9367f6b4cbedb1e172b97fdcbc5096bad8aae63",
    "zh:9d3bcaf076c2e0c429f6c5781b7ea5df36031d765846010d5df22ebd4bfc4246",
    "zh:af6ce5cdde9eaedfb13b51df7c028f8a11ac1d8af6be8a67155e63cf0f0b11e7",
    "zh:c7d33f1b6a6d2b8edb88522820feabb0a7c9673bbc63654ae8044a6ebd268e70",
    "zh:d1f98f2d4432ba6b7e40b0782a3b7c2a4fe48d5a8a68eeea0493f76f63f376c9",
    "zh:dc70306e02f809cbcff2192e442b81f701a80c352949a5e48226e1cf014065a3",
    "zh:e6b69eb2b72f8f712ca01dea3dec8fae0f8f2111e307d1b7b5763fcce591ea9c",
    "zh:f629bb272db68116e9e89cab624bf4f3dae8d77aa1e53e4af258de8324651034",
    "zh:fb6244c6f06255d1864e082cd56f8283d9ac71a544df40c943db7cf19a8a667f",
  ]
}

provider "registry.terraform.io/hashicorp/tfe" {
  version = "0.30.2"
  hashes = [
    "h1:sffzxgukfBmyWcf4rn+6a2eJzOlIESx4c6ueyl4mi5E=",
    "zh:1564f7d15e27ee0264f652928fe68b134c04e487ca9d21d4dc5945bd63d3a83f",
    "zh:1aeb73bc7c88e94586bfb43e4fc61b10dc1f9f973df017b720e20e26f9160708",
    "zh:36115e38eda2a99c7a10c6f51728b626a365961ca599957b5b32b0601fead7ac",
    "zh:6cf79632d3c8797044836f43491d9e6897f086b9ca2d1b9a6838483ab54842ee",
    "zh:6f03cd9912cceaa85676f9a7d3db629ba7eca5863e52c968ffbf9f41363e278f",
    "zh:76d768e0672f8241d5526a9f75f819d0a8f226c5a1f68bf1af4d3df29e12d93b",
    "zh:926d2ea43f81d9fc885944dd2af749745dd3cc4049cca82db6c7c2ff7dee5b1a",
    "zh:aa02a82738da90118806d0b16a907bcaff2cb5a5b87f10ad8058cd22da6dd77d",
    "zh:b8107f4636db54fa2216f27f9fa61ff87935bd55ac70a23de765eee0eaa4e4d6",
    "zh:e269ab5e60a84edb74233fa37bf6ce798e118fbe260f2a4364d5885047fbbcd4",
    "zh:ede767e70b43c9eb5c686736527dd0f082a6e3e56324676d30c9323d3cb655ac",
  ]
}
